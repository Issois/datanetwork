
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Geraet.DataNetwork{

	public class Receiver<T> : IDisposable{

		private bool disposed;
		public void Dispose(){
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing){
			if(this.disposed){return;}
			if(disposing){
				try{
					this.nq.Dispose();
				}catch(NullReferenceException){}
			}
			this.disposed = true;
		}


		public DiscardedItemOnFullQueue DiscardedItemOnFullQueue{
			get{return this.nq.DiscardedItemOnFullQueue;}
			set{this.nq.DiscardedItemOnFullQueue=value;}
		}

		private NetworkQueue<T> nq;
		public Func<T,T> Modify;

		public Receiver(int size=-1){
			this.Modify=i=>i;
			if(size<1){
				this.nq=new NetworkQueue<T>();
			}else{
				this.nq=new NetworkQueue<T>(size);
			}
		}

		public TaskInfo StartReceiving(Action<T> act){
			var cts=new CancellationTokenSource();
			var task=Task.Run(()=>{
				var token=cts.Token;
				T item;
				while(!token.IsCancellationRequested){
					item=this.NextItem(token);
					act(item);
				}
			},cts.Token);
			return new TaskInfo(task,cts);
		}


		public void Enqueue(T item){
			this.nq.Enqueue(item);
		}


		public T NextItem(){
			return this.NextItem(CancellationToken.None);
		}

		public T NextItem(CancellationToken token){
			return this.Modify(this.nq.Dequeue(token));
		}
	}
}