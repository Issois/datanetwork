
using System;
using System.Collections.Generic;

namespace Geraet.DataNetwork{

	public delegate T Converter<T>(params object[] args);

	public class Sender<T>{

		public const int DEFAULT_MAX_RECEIVER_COUNT=10;

		public Converter<T>? Extend{get;set;}
		public Func<T,T> Modify;


		private HashSet<Receiver<T>> receivers;

		public Sender(){
			this.Modify=i=>i;
			this.receivers=new HashSet<Receiver<T>>(DEFAULT_MAX_RECEIVER_COUNT);

		}

		public void Broadcast(params object[] args){
			if(args.Length==0){return;}
			if(this.Extend!=null){
				this.Enqueue(this.Extend(args));
			}else if(args[0] is T){
				this.Enqueue((T)args[0]);
			}
		}


		public void Unsubscribe(Receiver<T>[] receivers){
			foreach(var r in receivers){
				if(this.receivers.Contains(r)){
					this.receivers.Remove(r);
				}
			}
		}

		public void Subscribe(params Receiver<T>[] receivers){
			foreach(var r in receivers){
				if(!this.receivers.Contains(r)){
					this.receivers.Add(r);
				}
			}
		}

		private void Enqueue(T item){
			// var enqueuee=this.ModificationIsActivated?this.Modify(item):item;
			var enqueuee=this.Modify(item);
			foreach(var r in this.receivers){
				r.Enqueue(enqueuee);
			}
			// if(enqueuee!=null){
			// }
		}
	}
}