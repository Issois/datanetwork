
using System;
using System.Threading;
using System.Collections.Generic;

namespace Geraet.DataNetwork{

	public enum DiscardedItemOnFullQueue{Newest,Oldest}

	internal class NetworkQueue<T> : IDisposable{


		private bool disposed;
		public void Dispose(){
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing){
			if(this.disposed){return;}
			if(disposing){
				try{
					this.qAccess.Dispose();
					this.qNotEmptyAnymore.Dispose();
				}catch(NullReferenceException){}
			}
			this.disposed = true;
		}


		public const int DEFAULT_SIZE=100;

		private Queue<T> q;
		private SemaphoreSlim qAccess;
		private ManualResetEventSlim qNotEmptyAnymore;
		private int qSize;

		public DiscardedItemOnFullQueue DiscardedItemOnFullQueue{get;set;}

		public NetworkQueue(int size=DEFAULT_SIZE){
			this.qSize=size<1?1:size;
			this.q=new Queue<T>(this.qSize);
			this.qAccess=new SemaphoreSlim(1);
			this.qNotEmptyAnymore=new ManualResetEventSlim();
			this.DiscardedItemOnFullQueue=DiscardedItemOnFullQueue.Oldest;
		}

		public void Enqueue(T item){
			this.qAccess.Wait();
			if(q.Count==0){
				this.qNotEmptyAnymore.Set();
			}
			if(this.q.Count==this.qSize){
				if(this.DiscardedItemOnFullQueue==DiscardedItemOnFullQueue.Oldest){
					this.q.Dequeue();
					this.q.Enqueue(item);
				}
			}else{
				this.q.Enqueue(item);
			}
			this.qAccess.Release();
		}

		public T Dequeue(CancellationToken token){
			T item;
			this.qAccess.Wait();
			while(q.Count==0){
				this.qNotEmptyAnymore.Reset();
				this.qAccess.Release();
				this.qNotEmptyAnymore.Wait(token);
				this.qAccess.Wait();
			}
			item=this.q.Dequeue();
			this.qAccess.Release();
			return item;
		}
	}
}