
using System;
using System.Threading;

namespace Geraet.DataNetwork{

	internal static class ConsoleEntryPoint{

		public static void Main(string[] args){
			System.Console.WriteLine("Hello, World!");
			var s=new Sender<int>();
			s.Extend=(args)=>{
				int res=0;
				// int temp;
				foreach(var arg in args){
					if(arg is int){
						res+=(int)arg;
					}
				}
				return res;
			};
			var s2=new Sender<int>();
			// s.BroadcastX(1,2,3,4);
			var r=new Receiver<int>();
			var r2=new Receiver<int>();
			s.Subscribe(r,r2);
			s2.Subscribe(r,r2);
			var ti=r.StartReceiving((item)=>{System.Console.WriteLine("Received: "+(item));});
			r2.Modify=item=>item*2;
			var ti2=r2.StartReceiving((item)=>{System.Console.WriteLine("Received2: "+(item));});
			s.Broadcast(4);
			s2.Broadcast(40000);
			s.Broadcast(1,2,3,4);
			s.Broadcast(5);
			s2.Broadcast(40000,56);
			s.Broadcast(6);
			// Thread.Sleep(2000);
			ti.CancelAndJoin();
			ti2.CancelAndJoin();
			// ti.Task.Wait();
			System.Console.WriteLine("FINISH");
			// var s2=new Sender<string>(new Network<string>(),"s2");
			// s2.ModificationIsActivated=true;
			// s2.Modify=(str)=>{
			// 	System.Console.WriteLine(str);
			// 	return str;
			// 	return "s2: "+str;
			// };
			// s2.BroadcastX("nice","ööö");
			// s2.Broadcast("lol");
		}
	}
}