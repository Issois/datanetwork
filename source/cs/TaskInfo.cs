
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Geraet.DataNetwork{

	public class TaskInfo{
		public readonly Task Task;
		public readonly CancellationTokenSource CancellationTokenSource;
		public TaskInfo(Task task, CancellationTokenSource cancellationTokenSource){
			Task=task;
			CancellationTokenSource=cancellationTokenSource;
		}
		public void Cancel(){
			this.CancellationTokenSource.Cancel();
		}
		public void CancelAndJoin(){
			this.Cancel();
			try{
				this.Task.Wait();
			}catch(AggregateException){}
		}
	}
}