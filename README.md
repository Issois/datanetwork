# DataNetwork

Network for async data transfer.

### Usage
Use any type. For reference types only the reference will be send, not the object.

Each Receiver can be subscribed to multiple Senders and each Sender can broadcast to multiple Receivers.
**Beware that you will lose items if the speed of the Receiver can not keep up with the number of received items and the queue fills up.**

### Receiver
Receiver is IDisposable (It contains SemaphoreSlim and ManualResetEventSlim).
Add Receiver with size of its queue:
```
int queueSize=100
var r=new Receiver<int>(queueSize);
```
You can define a funtion to modify each received item:
```
r.Modify=item=>item+1;
```
Enqueue an item manually or Dequeue the oldest received item:
```
CancellationTokenSource cts;
r.Enqueue(3);
var item=r.NextItem(cts.token);
// item is now 4.
```
`NextItem` waits for a new item when the queue is empty. Cancel wait with a token.
You can automatically call NextItem in a Loop on a seperate thread with a defined action for each received item:
```
var info=r.StartReceiving((item)=>{System.Console.Write(item);});
```
This would print each received item. With the returned TaskInfo you have access to the spawned Task (`info.Task`) and the CancellationTokenSource.
```
info.CancelAndJoin();
// or
info.CancellationTokenSource.Cancel();
```
Set which item should be deleted when the queue is full: The first one in the queue (Oldest) or the new item that was received in this moment (Newest). Default is Oldest.
```
r.DiscardedItemOnFullQueue=DiscardedItemOnFullQueue.Oldest;
```

### Sender
Add Sender, Modify works the same as with the Recevier:
```
var s=new Sender<int>();
s.Modify=item=>item+3;
```
Subscribe multiple receivers to one sender (must have the same generic type):
```
s.Subscribe(r1,r2,r3);
```
Broadcast something:
```
s.Broadcast(5000);
```
The broadcasted item will be enqueued to each Receiver.
You can define the Extend-Property of a sender with a delegate with a params object array returning the generic type of the sender, to handle a Broadcast with multiple arguments:
```
s.Extend=(args)=>{
	if(args.Length>=2
			&& args[0] is int
			&& args[1] is bool){
		return (int)args[0]*((bool)args[1]?10:1000);
	}else{
		return 0;
	}
};
s.Broadcast(50,false);
// Broadcasts 50003.
s.Broadcast(50,true);
// Broadcasts 503.
s.Broadcast(50);
// Broadcasts 3.
// (Modify of Sender always adds 3)
```
The Senders modify will be applied after Extend (The other way would not work anyway).

